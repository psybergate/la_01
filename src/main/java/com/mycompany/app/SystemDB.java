package com.mycompany.app;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @since 21 Oct 2013
 */
public class SystemDB {

  private static final SystemDB INSTANCE = new SystemDB();

  private Map<String, Customer> customerDB = new HashMap<>();

  private SystemDB() {
  }

  public static SystemDB getInstance() {
    return INSTANCE;
  }

  public void addCustomer(Customer customer) {
    customerDB.put(customer.getCustomerNum(), customer);
    System.out.println(new Date() +  ": Added Customer with attributes =  " + customer);
  }

}
