package com.mycompany.app;

import java.util.Random;

/**
 * NOTE: ASSUME this class is used throughout all layers in the app
 * 
 * @author <a href=chris@psybergate.co.za>Chris Naidoo</a>
 * @since Feb 27, 2006
 */
public class UtilsRandom {

  // == Class Variables ================================================================================================
  private static final char[] CHARACTERS = {
      'a',
      'b',
      'c',
      'd',
      'e',
      'f',
      'g',
      'h',
      'i',
      'j',
      'k',
      'l',
      'm',
      'n',
      'o',
      'p',
      'q',
      'r',
      's',
      't',
      'u',
      'v',
      'w',
      'x',
      'y',
      'z',
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
      'V',
      'W',
      'X',
      'Y',
      'Z',
      '0',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9'};

  private static final int OFFSET_ALPHA_START = 0;

  private static final int OFFSET_ALPHA_END = 52;

  private static final int OFFSET_NUMERIC_START = 52;

  private static final int OFFSET_NUMERIC_END = CHARACTERS.length;

  private static final int OFFSET_ALPHA_NUMERIC_START = 0;

  private static final int OFFSET_ALPHA_NUMERIC_END = CHARACTERS.length;

  private static final Random RANDOM = new Random();

  // == Class Behaviour ================================================================================================
  /**
   * Generate a random string consisting of alphabetic characters.
   * 
   * @param length
   *          the required length of the generated string.
   * @return a randomly generated alphabetic string.
   */
  public static final String generateAlpha(int length) {
    return generate(OFFSET_ALPHA_START, OFFSET_ALPHA_END, length);
  }

  /**
   * Generate a random string consisting of numeric characters.
   * 
   * @param length
   *          the required length of the generated string.
   * @return a randomly generated numeric string.
   */
  public static final String generateNumeric(int length) {
    return generate(OFFSET_NUMERIC_START, OFFSET_NUMERIC_END, length);
  }

  /**
   * Generate a random string consisting of alphanumeric characters.
   * 
   * @param length
   *          the required length of the generated string.
   * @return a randomly generated alphanumeric string.
   */
  public static final String generateAlphaNumeric(int length) {
    return generate(OFFSET_ALPHA_NUMERIC_START, OFFSET_ALPHA_NUMERIC_END, length);
  }

  /**
   * Generates a random int between 0 and maxvalue.
   * 
   * @param maxValue
   *          - the upper limit for the random number that is to be generated.
   */
  public static final int generateNumber(int maxValue) {
    return (int) (((Math.random() + Math.random()) / 2) * maxValue);
  }

  /**
   * Generates a Long between 0 and maxvalue.
   * 
   * @param maxValue
   *          - the upper limit for the random number that is to be generated.
   */
  public static final Long generateLong(int maxValue) {
    return new Long((int) (((Math.random() + Math.random()) / 2) * maxValue));
  }

  /**
   * Generate a random string containing characters from the specified range of elements in the <code>CHARACTERS</code>
   * lookup table.
   * 
   * @param startOffset
   *          the start offset in the lookup table.
   * @param endOffset
   *          the end offset in the lookup table
   * @param length
   *          the required length of the generated string.
   * @return a randomly generated string containing the specified range of characters.
   */
  private static final String generate(int startOffset, int endOffset, int length) {
    char[] result = new char[length];
    int range = endOffset - startOffset;
    for (int k = 0; k < length; k++) {
      result[k] = CHARACTERS[startOffset + RANDOM.nextInt(range)];
    }
    return new String(result);
  }
}
