package com.mycompany.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @since 15 Oct 2010
 */
public interface Controller {

  /**
   * @return the viewName to go to next
   */
  String execute(HttpServletRequest request, HttpServletResponse response);

}
