package com.mycompany.app;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @since 15 Oct 2010
 */
public class CustomerController
    implements Controller {

  private CustomerService customerService = new CustomerServiceImpl();

  public String execute(HttpServletRequest request, HttpServletResponse response) {
    String pathInfo = request.getPathInfo();
    if (pathInfo.equals("/addcustomer_view")) {
      return processEventAddCustomer_View(request, response);
    }
    else if (pathInfo.equals("/addcustomer_add")) {
      return processEventAddCustomer_Add(request, response);

    }
    else {
      throw new RuntimeException("PathInfo : " + pathInfo + " not handled by this controller.");
    }

  }

  private String processEventAddCustomer_View(HttpServletRequest request, HttpServletResponse response) {
    return "addcustomer_view";
  }

  private String processEventAddCustomer_Add(HttpServletRequest request, HttpServletResponse response) {
    Customer customer = new Customer();
    bindCustomer(request, customer);
    getCustomerService().addCustomer(customer);
    request.setAttribute("message", "Customer successfully added!");
    return "addcustomer_view";
  }

  private void bindCustomer(HttpServletRequest request, Customer customer) {
    String dateOfBirthAsString = null;
    try {
      customer.setCustomerNum(request.getParameter("customerNum"));
      customer.setFirstName(request.getParameter("firstName"));
      customer.setSurname(request.getParameter("surname"));
      dateOfBirthAsString = request.getParameter("dateOfBirth");
      DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
      dateFormat.setLenient(true);
      customer.setDateOfBirth(dateFormat.parse(dateOfBirthAsString));
    }
    catch (ParseException ex) {
      throw new RuntimeException("Error parsing date : " + dateOfBirthAsString);
    }
  }

  public CustomerService getCustomerService() {
    return customerService;
  }

}
