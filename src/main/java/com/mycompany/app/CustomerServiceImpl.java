package com.mycompany.app;


/**
 * 
 * @since 10 Oct 2010
 */
public class CustomerServiceImpl
    implements CustomerService {

  private CustomerResource customerResource = new CustomerResourceImpl();

  public CustomerServiceImpl() {

  }

  public Customer addCustomer(Customer customer) {
    return getCustomerResource().addCustomer(customer);
  }

  private CustomerResource getCustomerResource() {
    return customerResource;
  }
}
