package com.mycompany.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @since 10 Oct 2010
 */
public class DispatcherServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  public static final String VERSION = "Dispatcher-0.01>>";

  private static final String DEFAULT_PATHINFO = "/addcustomer_view";

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  static {
    CONTROLLERS.put("/addcustomer_view", new CustomerController());
    CONTROLLERS.put("/addcustomer_add", new CustomerController());
  }

  public void init()
      throws ServletException {
    super.init();
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    print("Entering doGet()");
    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/addcustomer_view.jsp");
    try {

      String pathInfo = request.getPathInfo();
      String viewName = null;
      if (pathInfo == null) {
        pathInfo = DEFAULT_PATHINFO;
      }
      print("Processing pathinfo : " + pathInfo);
      Controller controller = getController(pathInfo);
      if (controller == null) {
        print("Throwing runtimeexception for pathinfo : " + pathInfo);
        throw new RuntimeException("PathInfo : " + pathInfo + " not handled by this servlet");
      }

      viewName = controller.execute(request, response);
      if (viewName == null) {
        throw new RuntimeException("Invalid viewname : viewname = null");
      }

      dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/" + viewName + ".jsp");
    }
    catch (Exception ex) {
      request.setAttribute("message", ex.getMessage());
    }
    dispatcher.forward(request, response);

  }

  public Controller getController(String pathInfo) {
    return CONTROLLERS.get(pathInfo);
  }

  private static void print(String message) {
    System.out.println(VERSION + message);
  }

}
