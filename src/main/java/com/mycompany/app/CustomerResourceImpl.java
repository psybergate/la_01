package com.mycompany.app;


/**
 * 
 * @since 10 Oct 2010
 */
public class CustomerResourceImpl
    implements CustomerResource {

  private static int startId = (int) UtilsRandom.generateNumber(100000000);

  private static int id = 0;

  private SystemDB systemDB = SystemDB.getInstance();

  public CustomerResourceImpl() {
  }

  public Customer addCustomer(Customer customer) {
    customer.setId(startId + ++id);
    systemDB.addCustomer(customer);
    return customer;
  }

}
