package com.mycompany.app;

import java.util.Date;

/**
 * 
 * @since 10 Oct 2010
 */
public class Customer {

  private int id;

  private String customerNum;

  private String firstName;

  private String surname;

  private Date dateOfBirth;

  public Customer() {
  }

  public Customer(String customerNum, String firstName, String surname, Date dateOfBirth) {
    this.customerNum = customerNum;
    this.firstName = firstName;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  @Override
  public String toString() {
    return "Customer [id=" + id + ", customerNum=" + customerNum + ", firstName=" + firstName + ", surname=" + surname
        + ", dateOfBirth=" + dateOfBirth + "]";
  }

}
