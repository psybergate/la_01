package com.mycompany.app;

import java.util.GregorianCalendar;

import org.junit.Test;

import com.mycompany.app.Customer;
import com.mycompany.app.CustomerService;
import com.mycompany.app.CustomerServiceImpl;
import com.mycompany.app.UtilsRandom;

/**
 * 
 * @since 10 Oct 2010
 */
public class CustomerServiceTest {

  private CustomerService customerService = new CustomerServiceImpl();

  @Test
  public void testAddCustomer() {
    String customerNum = UtilsRandom.generateAlpha(4);

    Customer customer = new Customer(customerNum, "Chris-999", "Naidoo", new GregorianCalendar(2000, 1, 1).getTime());
    getCustomerService().addCustomer(customer);
  }

  public CustomerService getCustomerService() {
    return customerService;
  }

}
